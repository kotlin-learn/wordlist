package com.example.wordsapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.wordsapp.databinding.FragmentWordListBinding


class WordListFragment : Fragment() {
    // TODO: Rename and change types of parameters
    var _binding: FragmentWordListBinding? = null
    val binding get() = _binding
    private lateinit var recyclerView: RecyclerView
    private lateinit var letterId: String

    //Oncreate -> onCreaateView -> onViewCreated
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    //        ไม่มีmenu ไม่ต้อง setHasOption()
        letterId = arguments?.getString(LETTER).toString() //รับค่าจากฝั่งนู็นมา
//         arguments?.let {
//            letterId = it.getString(LETTER).toString() }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding=FragmentWordListBinding.inflate(inflater,container,false)
        val view = binding?.root
        return view
    }
    //set components ต่างๆตรงนี้
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = binding?.recyclerView!!
        recyclerView.layoutManager=LinearLayoutManager(context)
        recyclerView.adapter = WordAdapter(letterId,this.requireContext())
        //เพิ่ม dividerItemdecoration ระหว่าง items
        recyclerView.addItemDecoration(
            DividerItemDecoration(context,DividerItemDecoration.VERTICAL)

        )

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val LETTER = "letter"
        const val SEARCH_PREFIX = "https://www.google.com/search?q="
    }
}