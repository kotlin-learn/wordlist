package com.example.wordsapp

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.wordsapp.databinding.FragmentLetterListBinding

class LetterListFragment : Fragment() {
    // TODO: Rename and change types of parameters

    //-1 _binding
    private var _binding: FragmentLetterListBinding? = null;
    private val binding get() = _binding
    //-4 recyclerview
    private lateinit  var recyclerView: RecyclerView;
    //-7
    private var isLinearLayoutManager = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true) //-2 มี options ต้องใส่คำสั่งนี้ด้วย
    }
    /*
    onCraeted -> สร้างตัวแปรสำหรับ Fragement
    oncreateView  -> เอาตัวแปรมา binding ก่อน
    onViewCrated ->  เอา comopnents ต่างๆมา binding
    */

    //3 bind XML
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //ตัว binding มา inflate กับ container
        _binding = FragmentLetterListBinding.inflate(inflater,container,false)
        // Inflate the layout for this fragment
        val view = binding?.root
        return view
    }


    //-5 overide method นีี้มา
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView= binding?.recyclerView!!
        chooseLayout()
    }

    // -8 override metthod
    override fun onDestroy() {
        super.onDestroy()
        _binding=null
    }

    //-6 chooseLayout()
    private fun chooseLayout() {
        if (isLinearLayoutManager) {
            recyclerView.layoutManager = LinearLayoutManager(context)
        } else {
            recyclerView.layoutManager = GridLayoutManager(context, 4)
        }
        recyclerView.adapter = LetterAdapter()
    }

    override fun onCreateOptionsMenu(menu: Menu,inflater: MenuInflater) {
        // Fragtement ไม่มี menuInflater เปนของตัวเอง
        inflater.inflate(R.menu.layout_menu, menu)

        val layoutButton = menu?.findItem(R.id.action_switch_layout)
        // Calls code to set the icon based on the LinearLayoutManager of the RecyclerView
        setIcon(layoutButton)

    }

    private fun setIcon(menuItem: MenuItem?) {
        if (menuItem == null)
            return

        menuItem.icon =
            if (isLinearLayoutManager)
                //this.requireContext() จะreturn context ที่ fragement ตัวนั้นใช้อยู่
                ContextCompat.getDrawable(this.requireContext(), R.drawable.ic_grid_layout)
            else ContextCompat.getDrawable(this.requireContext(), R.drawable.ic_linear_layout)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_switch_layout -> {
                // Sets isLinearLayoutManager (a Boolean) to the opposite value
                isLinearLayoutManager = !isLinearLayoutManager
                // Sets layout and icon
                chooseLayout()
                setIcon(item)

                return true
            }
            //  Otherwise, do nothing and use the core event handling

            // when clauses require that all possible paths be accounted for explicitly,
            //  for instance both the true and false cases if the value is a Boolean,
            //  or an else to catch all unhandled cases.
            else -> super.onOptionsItemSelected(item)
        }
    }


}